@extends('layout.master')

@section('title')
    Add Cast Member
@endsection

@section('content')
<div>
    <form action="/cast" method="POST">
        @csrf
        <div class="form-group">
            <label for="title">Name</label>
            <input type="text" class="form-control" name="nama" id="title" placeholder="Input Name">
            @error('nama')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="body">Age</label>
            <input type="number" class="form-control" name="umur" id="body" placeholder="Input Age">
            @error('umur')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="title">Bio</label>
            <input type="text" class="form-control" name="bio" id="title" placeholder="Input Bio">
            @error('bio')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary">Add</button>
    </form>
</div>
@endsection