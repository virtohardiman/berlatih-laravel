@extends('layout.master')

@section('title')
    Tambah Cast
@endsection

@section('content')
<div>
    <form action="/cast/{{$cast->id}}" method="POST">
        @csrf
        @method('put')
        <div class="form-group">
            <label for="title">Name</label>
            <input type="text" class="form-control" value="{{$cast->nama}}" name="nama" id="title" placeholder="Input Name">
            @error('nama')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="body">Age</label>
            <input type="number" class="form-control" value="{{$cast->umur}}" name="umur" id="body" placeholder="Input Age">
            @error('umur')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="title">Bio</label>
            <input type="text" class="form-control" value="{{$cast->bio}}" name="bio" id="title" placeholder="Input Bio">
            @error('bio')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary">Edit</button>
    </form>
</div>
@endsection