@extends('layout.master')

@section('title')
    Halaman Form
@endsection

@section('content')
    <form action="welcome" method="POST">
        @csrf
      <label for="first_name">First name:</label>
      <br /><br />
      <input id="first_name" type="text" name="first_name" required />
      <br /><br />
      <label for="last_name">Last name:</label>
      <br /><br />
      <input id="last_name" type="text" name="last_name" required />
      <br /><br />
      <label>Gender:</label>
      <br /><br />
      <input type="radio" name="gender" value="male" required="required" />Male
      <br />
      <input type="radio" name="gender" value="female" required="required" />Female
      <br />
      <input type="radio" name="gender" value="other" required="required" />Other<br />
      <br />
      <label>Nationality:</label>
      <br /><br />
      <select name="nationality">
        <option value="1">Indonesian</option>
        <option value="2">Singaporean</option>
        <option value="2">Malaysian</option>
        <option value="2">Australian</option>
      </select>
      <br /><br />
      <label>Language Spoken</label>
      <br /><br />
      <input type="checkbox" name="language" />Bahasa Indonesia
      <br />
      <input type="checkbox" name="language" />English
      <br />
      <input type="checkbox" name="language" />Other <br /><br />
      <label for="biography">Bio:</label>
      <br /><br />
      <textarea name="biography" cols="34" rows="10"></textarea>
      <br />
      <input type="submit" value="Sign Up" />
    </form>

@endsection
